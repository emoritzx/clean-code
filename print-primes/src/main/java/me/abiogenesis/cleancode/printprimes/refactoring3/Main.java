package me.abiogenesis.cleancode.printprimes.refactoring3;

import java.util.List;

public class Main {

    public static void main(String[] args) {

        int numberOfPrimes = Integer.parseInt(args[0]);
        PrimeGenerator primeGenerator = new OriginalPrimeGenerator();
        TablePrinter tablePrinter = new OriginalTablePrinter();

        List<Integer> primes = primeGenerator.generate(numberOfPrimes);
        tablePrinter.printTable(primes);
    }
}
