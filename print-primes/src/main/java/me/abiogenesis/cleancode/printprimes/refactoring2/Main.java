package me.abiogenesis.cleancode.printprimes.refactoring2;

import java.util.List;

public class Main {

    public static void main(String[] args) {
        int numberOfPrimes = Integer.parseInt(args[0]);
        List<Integer> primes = PrimeGenerator.generate(numberOfPrimes);
        TablePrinter.printTable(primes);
    }
}
