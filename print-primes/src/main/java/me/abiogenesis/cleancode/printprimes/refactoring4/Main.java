package me.abiogenesis.cleancode.printprimes.refactoring4;

import java.util.List;

public class Main {

    public static void main(String[] args) {

        int numberOfPrimes = Integer.parseInt(args[0]);
        int columnsPerPage = Integer.parseInt(args[1]);
        int rowsPerPage = Integer.parseInt(args[2]);

        PrimeGenerator primeGenerator = new RefactoredPrimeGenerator();
        TablePrinter tablePrinter = new RefactoredTablePrinter(System.out, columnsPerPage, rowsPerPage);

        List<Integer> primes = primeGenerator.generate(numberOfPrimes);
        tablePrinter.printTable(primes);
    }
}
