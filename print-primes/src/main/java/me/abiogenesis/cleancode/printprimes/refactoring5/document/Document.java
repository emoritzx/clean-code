package me.abiogenesis.cleancode.printprimes.refactoring5.document;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Document<DataType> {

    protected final DocumentConfiguration configuration;
    protected final List<DataType> data;

    public Document(DocumentConfiguration configuration, List<DataType> data) {
        this.configuration = configuration;
        this.data = Collections.unmodifiableList(data);
    }

    public int getNumberOfItems() {
        return data.size();
    }

    public List<Page<DataType>> getPages() {
        return IntStream.rangeClosed(1, getNumberOfPages())
            .mapToObj(this::getPage)
            .collect(Collectors.toList());
    }

    public Page<DataType> getPage(int pageNumber) {
        return new Page<>(this, pageNumber);
    }

    public int getNumberOfPages() {
        return (int) Math.ceil(data.size() / (double) configuration.getPageSize());
    }
}
