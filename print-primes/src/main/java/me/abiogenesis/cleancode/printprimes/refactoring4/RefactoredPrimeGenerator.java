package me.abiogenesis.cleancode.printprimes.refactoring4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Refactoring #4
 *   - Applied the refactorings of Listing 10-8
 *   - Pass around an intermediate storage class instead of static variables
 */
public class RefactoredPrimeGenerator implements PrimeGenerator {

    @Override
    public List<Integer> generate(int numberOfPrimes) {
        PrimeNumberStorage storage = new PrimeNumberStorage(numberOfPrimes);
        checkOddNumbersForSubsequentPrimes(storage);
        return Arrays.stream(storage.primes)
            .skip(1)
            .boxed()
            .collect(Collectors.toList());
    }

    private static void checkOddNumbersForSubsequentPrimes(PrimeNumberStorage storage) {
        int primeIndex = 1;
        for (int candidate = 3; primeIndex < storage.primes.length; candidate += 2) {
            if (isPrime(storage, candidate)) {
                storage.primes[primeIndex++] = candidate;
            }
        }
    }

    private static boolean isPrime(PrimeNumberStorage storage, int candidate) {
        if (isLeastRelevantMultipleOfNextLargerPrimeFactor(storage, candidate)) {
            storage.multiplesOfPrimeFactors.add(candidate);
            return false;
        }
        return isNotMultipleOfAnyPreviousPrimeFactor(storage, candidate);
    }

    private static boolean isLeastRelevantMultipleOfNextLargerPrimeFactor(PrimeNumberStorage storage, int candidate) {
        int nextLargerPrimeFactor = storage.primes[storage.multiplesOfPrimeFactors.size()];
        int leastRelevantMultiple = nextLargerPrimeFactor * nextLargerPrimeFactor;
        return candidate == leastRelevantMultiple;
    }

    private static boolean isNotMultipleOfAnyPreviousPrimeFactor(PrimeNumberStorage storage, int candidate) {
        for (int n = 1; n < storage.multiplesOfPrimeFactors.size(); n++) {
            if (isMultipleOfNthPrimeFactor(storage, candidate, n)) {
                return false;
            }
        }
        return true;
    }

    private static boolean isMultipleOfNthPrimeFactor(PrimeNumberStorage storage, int candidate, int n) {
        return candidate == smallestOddNthMultipleNotLessThanCandidate(storage, candidate, n);
    }

    private static int smallestOddNthMultipleNotLessThanCandidate(PrimeNumberStorage storage, int candidate, int n) {
        int multiple = storage.multiplesOfPrimeFactors.get(n);
        while (multiple < candidate) {
            multiple += 2 * storage.primes[n];
        }
        storage.multiplesOfPrimeFactors.set(n, multiple);
        return multiple;
    }

    private static class PrimeNumberStorage {

        static final int INITIAL_PRIME = 2;

        final int[] primes;
        final List<Integer> multiplesOfPrimeFactors = new ArrayList<>();

        public PrimeNumberStorage(int numberOfPrimes) {
            primes = new int[numberOfPrimes + 1];
            primes[1] = INITIAL_PRIME;
            multiplesOfPrimeFactors.add(INITIAL_PRIME);
        }
    }
}
