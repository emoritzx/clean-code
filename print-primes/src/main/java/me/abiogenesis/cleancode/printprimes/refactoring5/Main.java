package me.abiogenesis.cleancode.printprimes.refactoring5;

import me.abiogenesis.cleancode.printprimes.refactoring5.document.Document;
import me.abiogenesis.cleancode.printprimes.refactoring5.document.DocumentConfiguration;

import java.util.List;

public class Main {

    public static void main(String[] args) {

        int numberOfPrimes = Integer.parseInt(args[0]);
        int columnsPerPage = Integer.parseInt(args[1]);
        int rowsPerPage = Integer.parseInt(args[2]);

        DocumentConfiguration configuration = new DocumentConfiguration(columnsPerPage, rowsPerPage);
        PrimeGenerator primeGenerator = new RefactoredPrimeGenerator();
        TablePrinter tablePrinter = new RefactoredTablePrinter(System.out);

        List<Integer> primes = primeGenerator.generate(numberOfPrimes);
        Document<Integer> primesView = new Document<>(configuration, primes);
        tablePrinter.printTable(primesView);
    }
}
