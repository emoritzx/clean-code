/**
 * Refactoring #5 - extract data structures to separate representation from formatting
 */
package me.abiogenesis.cleancode.printprimes.refactoring5;