package me.abiogenesis.cleancode.printprimes.refactoring3;

import java.util.List;

public interface TablePrinter {

    void printTable(List<Integer> primes);
}
