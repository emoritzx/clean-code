package me.abiogenesis.cleancode.printprimes.refactoring5;

import me.abiogenesis.cleancode.printprimes.refactoring5.document.Document;

public interface TablePrinter {

    void printTable(Document<Integer> document);
}
