package me.abiogenesis.cleancode.printprimes.refactoring1;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class PrintPrimes {

    public static void main(String[] args) {
        int numberOfPrimes = Integer.parseInt(args[0]);
        List<Integer> primes = calculatePrimes(numberOfPrimes);
        printTable(primes);
    }

    public static List<Integer> calculatePrimes(int numberOfPrimes) {

        final int ORDMAX = 30;
        int P[] = new int[numberOfPrimes + 1];

        int J;
        int K;
        boolean JPRIME;
        int ORD;
        int SQUARE;
        int N;
        int MULT[] = new int[ORDMAX + 1];

        J = 1;
        K = 1;
        P[1] = 2;
        ORD = 2;
        SQUARE = 9;

        while (K < numberOfPrimes) {
            do {
                J = J + 2;
                if (J == SQUARE) {
                    ORD = ORD + 1;
                    SQUARE = P[ORD] * P[ORD];
                    MULT[ORD - 1] = J;
                }
                N = 2;
                JPRIME = true;
                while (N < ORD && JPRIME) {
                    while (MULT[N] < J)
                        MULT[N] = MULT[N] + P[N] + P[N];
                    if (MULT[N] == J)
                        JPRIME = false;
                    N = N + 1;
                }
            } while (!JPRIME);
            K = K + 1;
            P[K] = J;
        }

        return Arrays.stream(P)
            .skip(1)
            .boxed()
            .collect(Collectors.toList());
    }

    public static void printTable(List<Integer> primes) {
        final int M = primes.size();
        final int RR = 50;
        final int CC = 4;
        int PAGENUMBER;
        int PAGEOFFSET;
        int ROWOFFSET;
        int C;
        PAGENUMBER = 1;
        PAGEOFFSET = 1;
        while (PAGEOFFSET < M) {
            System.out.println("The First " + M +
                " Prime Numbers --- Page " + PAGENUMBER);
            System.out.println("");
            for (ROWOFFSET = PAGEOFFSET; ROWOFFSET < PAGEOFFSET + RR; ROWOFFSET++) {
                for (C = 0; C < CC; C++)
                    if (ROWOFFSET + C * RR < M)
                        System.out.format("%10d", primes.get(ROWOFFSET + C * RR));
                System.out.println("");
            }
            System.out.println("\f");
            PAGENUMBER = PAGENUMBER + 1;
            PAGEOFFSET = PAGEOFFSET + RR * CC;
        }
    }
}
