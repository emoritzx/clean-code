package me.abiogenesis.cleancode.printprimes.refactoring3;

import java.util.List;

public class OriginalTablePrinter implements TablePrinter {

    @Override
    public void printTable(List<Integer> primes) {
        final int M = primes.size();
        final int RR = 50;
        final int CC = 4;
        int PAGENUMBER;
        int PAGEOFFSET;
        int ROWOFFSET;
        int C;
        PAGENUMBER = 1;
        PAGEOFFSET = 1;
        while (PAGEOFFSET <= M) {
            System.out.println("The First " + M +
                " Prime Numbers --- Page " + PAGENUMBER);
            System.out.println("");
            for (ROWOFFSET = PAGEOFFSET; ROWOFFSET < PAGEOFFSET + RR; ROWOFFSET++) {
                for (C = 0; C < CC; C++)
                    if (ROWOFFSET + C * RR <= M)
                        System.out.format("%10d", primes.get(ROWOFFSET + C * RR));
                System.out.println("");
            }
            System.out.println("\f");
            PAGENUMBER = PAGENUMBER + 1;
            PAGEOFFSET = PAGEOFFSET + RR * CC;
        }
    }
}
