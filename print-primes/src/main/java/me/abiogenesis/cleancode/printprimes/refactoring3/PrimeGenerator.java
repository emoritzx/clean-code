package me.abiogenesis.cleancode.printprimes.refactoring3;

import java.util.List;

public interface PrimeGenerator {

    List<Integer> generate(int numberOfPrimes);
}
