package me.abiogenesis.cleancode.printprimes.refactoring5.document;

public class DocumentConfiguration {

    public final int columnsPerPage;
    public final int rowsPerPage;

    public DocumentConfiguration(int columnsPerPage, int rowsPerPage) {
        this.columnsPerPage = columnsPerPage;
        this.rowsPerPage = rowsPerPage;
    }

    public int getPageSize() {
        return columnsPerPage * rowsPerPage;
    }
}
