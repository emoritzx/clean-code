package me.abiogenesis.cleancode.printprimes.refactoring4;

import java.io.PrintStream;
import java.util.List;
import java.util.stream.IntStream;

/**
 * Refactoring #4
 * - made rows and columns configurable
 * - used Dependency Inversion to inject the output stream instead of hard-coding System.out
 * - extracted behaviors as private functions
 * - renamed variables
 * - general formatting
 */
public class RefactoredTablePrinter implements TablePrinter {

    private final PrintStream outputStream;

    private final int columnsPerPage;
    private final int rowsPerPage;

    public RefactoredTablePrinter(PrintStream outputStream, int columnsPerPage, int rowsPerPage) {
        this.outputStream = outputStream;
        this.columnsPerPage = columnsPerPage;
        this.rowsPerPage = rowsPerPage;
    }

    @Override
    public void printTable(List<Integer> primes) {
        final int numberOfPrimes = primes.size();
        final int numberOfPages = calculateNumberOfPages(numberOfPrimes);
        IntStream.rangeClosed(1, numberOfPages)
            .forEachOrdered(pageNumber -> printPage(pageNumber, primes));
    }

    private int calculateNumberOfPages(int numberOfPrimes) {
        final int itemsPerPage = calculatePageSize();
        return (int) Math.ceil(numberOfPrimes / (double) itemsPerPage);
    }

    private int calculatePageSize() {
        return rowsPerPage * columnsPerPage;
    }

    private void printPage(int pageNumber, List<Integer> primes) {
        final int numberOfPrimes = primes.size();
        outputStream.printf("The First %d Prime Numbers --- Page %d%n", numberOfPrimes, pageNumber);
        List<Integer> pageView = extractPage(pageNumber, primes);
        final int numberOfRows = calculateNumberOfRows(pageView);
        IntStream.range(0, numberOfRows)
            .forEachOrdered(row -> printRow(row, pageView));
    }

    private List<Integer> extractPage(int pageNumber, List<Integer> primes) {
        final int pageOffset = calculatePageOffset(pageNumber);
        final int pageSize = calculatePageSize();
        final int remainingItems = primes.size() - pageOffset;
        final int endIndex = pageOffset + Math.min(pageSize, remainingItems);
        return primes.subList(pageOffset, endIndex);
    }

    private int calculatePageOffset(int pageNumber) {
        return (pageNumber - 1) * calculatePageSize();
    }

    private int calculateNumberOfRows(List<Integer> pageView) {
        return (int) Math.ceil(pageView.size() / (double) columnsPerPage);
    }

    private void printRow(int rowNumber, List<Integer> pageView) {
        List<Integer> rowView = extractRow(rowNumber, pageView);
        rowView.forEach(this::printItem);
        outputStream.println();
    }

    private List<Integer> extractRow(int rowNumber, List<Integer> pageView) {
        final int rowOffset = calculateRowOffset(rowNumber);
        final int remainingItems = pageView.size() - rowOffset;
        final int endIndex = rowOffset + Math.min(columnsPerPage, remainingItems);
        return pageView.subList(rowOffset, endIndex);
    }

    private int calculateRowOffset(int rowNumber) {
        return rowNumber * columnsPerPage;
    }

    private void printItem(int item) {
        outputStream.format("%10d", item);
    }
}
