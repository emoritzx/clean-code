package me.abiogenesis.cleancode.printprimes.refactoring4;

import java.util.List;

public interface TablePrinter {

    void printTable(List<Integer> primes);
}
