package me.abiogenesis.cleancode.printprimes.refactoring5;

import java.util.List;

public interface PrimeGenerator {

    List<Integer> generate(int numberOfPrimes);
}
