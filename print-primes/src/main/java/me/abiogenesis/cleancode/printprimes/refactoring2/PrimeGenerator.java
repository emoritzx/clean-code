package me.abiogenesis.cleancode.printprimes.refactoring2;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class PrimeGenerator {

    public static List<Integer> generate(int numberOfPrimes) {

        final int ORDMAX = 30;
        int P[] = new int[numberOfPrimes + 1];

        int J;
        int K;
        boolean JPRIME;
        int ORD;
        int SQUARE;
        int N;
        int MULT[] = new int[ORDMAX + 1];

        J = 1;
        K = 1;
        P[1] = 2;
        ORD = 2;
        SQUARE = 9;

        while (K < numberOfPrimes) {
            do {
                J = J + 2;
                if (J == SQUARE) {
                    ORD = ORD + 1;
                    SQUARE = P[ORD] * P[ORD];
                    MULT[ORD - 1] = J;
                }
                N = 2;
                JPRIME = true;
                while (N < ORD && JPRIME) {
                    while (MULT[N] < J)
                        MULT[N] = MULT[N] + P[N] + P[N];
                    if (MULT[N] == J)
                        JPRIME = false;
                    N = N + 1;
                }
            } while (!JPRIME);
            K = K + 1;
            P[K] = J;
        }

        return Arrays.stream(P)
            .skip(1)
            .boxed()
            .collect(Collectors.toList());
    }
}
