package me.abiogenesis.cleancode.printprimes.refactoring5.document;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Page<DataType> {

    private final Document<DataType> document;
    private final int pageNumber;
    private final int pageStartIndex;
    private final int pageEndIndex;

    Page(Document<DataType> document, int pageNumber) {
        this.document = document;
        this.pageNumber = pageNumber;
        this.pageStartIndex = calculateStartIndex();
        this.pageEndIndex = calculateEndIndex();
    }

    public int getNumber() {
        return pageNumber;
    }

    public int getNumberOfRows() {
        return (int) Math.ceil((pageEndIndex - pageStartIndex) / (double) document.configuration.columnsPerPage);
    }

    public List<List<DataType>> getRows() {
        return IntStream.range(0, getNumberOfRows())
            .mapToObj(this::getRow)
            .collect(Collectors.toList());
    }

    public List<DataType> getRow(int rowIndex) {
        final int rowOffset = calculateRowOffset(rowIndex);
        final int remainingItems = pageEndIndex - rowOffset;
        final int rowEndIndex = rowOffset + Math.min(document.configuration.columnsPerPage, remainingItems);
        return document.data.subList(rowOffset, rowEndIndex);
    }

    private int calculateStartIndex() {
        return (pageNumber - 1) * document.configuration.getPageSize();
    }

    private int calculateEndIndex() {
        final int remainingItems = document.data.size() - pageStartIndex;
        return pageStartIndex + Math.min(document.configuration.getPageSize(), remainingItems);
    }

    private int calculateRowOffset(int rowIndex) {
        return pageStartIndex + (rowIndex * document.configuration.columnsPerPage);
    }
}
