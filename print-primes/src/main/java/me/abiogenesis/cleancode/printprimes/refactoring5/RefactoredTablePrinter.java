package me.abiogenesis.cleancode.printprimes.refactoring5;

import me.abiogenesis.cleancode.printprimes.refactoring5.document.Document;
import me.abiogenesis.cleancode.printprimes.refactoring5.document.Page;

import java.io.PrintStream;
import java.util.List;

public class RefactoredTablePrinter implements TablePrinter {

    private final PrintStream outputStream;

    public RefactoredTablePrinter(PrintStream outputStream) {
        this.outputStream = outputStream;
    }

    @Override
    public void printTable(Document<Integer> document) {
        document.getPages()
            .forEach(page -> printPage(document.getNumberOfItems(), page));
    }

    private void printPage(int numberOfPrimes, Page<Integer> page) {
        outputStream.printf("The First %d Prime Numbers --- Page %d%n", numberOfPrimes, page.getNumber());
        page.getRows()
            .forEach(this::printRow);
    }

    private void printRow(List<Integer> rowView) {
        rowView.forEach(this::printItem);
        outputStream.println();
    }

    private void printItem(int item) {
        outputStream.format("%10d", item);
    }
}
