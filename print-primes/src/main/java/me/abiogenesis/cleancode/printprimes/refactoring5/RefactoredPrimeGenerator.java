package me.abiogenesis.cleancode.printprimes.refactoring5;

import java.util.List;
import java.util.function.IntPredicate;
import java.util.function.IntUnaryOperator;
import java.util.stream.IntStream;

public class RefactoredPrimeGenerator implements PrimeGenerator {

    @Override
    public List<Integer> generate(int numberOfPrimes) {
        PrimeNumberStorage storage = new PrimeNumberStorage();
        checkOddNumbersForSubsequentPrimes(storage, numberOfPrimes);
        return storage.getPrimes();
    }

    private static void checkOddNumbersForSubsequentPrimes(PrimeNumberStorage storage, int numberOfPrimes) {

        // this wasn't necessary, but I wanted to see what a for-loop looked like with the Java Stream API

        final int startingWithTheFirstOddPrime = 3;
        final IntPredicate whileTheCurrentNumberOfPrimesIsLessThanRequested = unused -> storage.getPrimes().size() < numberOfPrimes;
        final IntUnaryOperator getNextOddNumber = previousOddNumber -> previousOddNumber + 2;

        IntStream.iterate(startingWithTheFirstOddPrime, whileTheCurrentNumberOfPrimesIsLessThanRequested, getNextOddNumber)
            .forEachOrdered(storage::checkPrime);
    }
}
