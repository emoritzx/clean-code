package me.abiogenesis.cleancode.printprimes.refactoring5;

import java.util.ArrayList;
import java.util.List;

public class PrimeNumberStorage {

    static final int INITIAL_PRIME = 2;

    private final List<Integer> primes;
    private final List<Integer> multiplesOfPrimeFactors = new ArrayList<>();

    public PrimeNumberStorage() {
        primes = new ArrayList<>();
        primes.add(0); // stupid 1-based logic
        primes.add(INITIAL_PRIME);
        multiplesOfPrimeFactors.add(INITIAL_PRIME);
    }

    public void checkPrime(int candidate) {
        System.out.println("candidate: " + candidate);
        if (isLeastRelevantMultipleOfNextLargerPrimeFactor(candidate)) {
            System.out.println("isLeastRelevant");
            multiplesOfPrimeFactors.add(candidate);
        } else if (isNotMultipleOfAnyPreviousPrimeFactor(candidate)) {
            System.out.println("isNotMultiple");
            primes.add(candidate);
        } else {
            System.out.println("isMultiple");
        }
        System.out.print("primes:");
        primes.forEach(prime -> System.out.printf(" %d", prime));
        System.out.println();
    }

    public List<Integer> getPrimes() {
        return primes.subList(1, primes.size());
    }

    private boolean isLeastRelevantMultipleOfNextLargerPrimeFactor(int candidate) {
        int nextLargerPrimeFactor = primes.get(multiplesOfPrimeFactors.size());
        int leastRelevantMultiple = nextLargerPrimeFactor * nextLargerPrimeFactor;
        return candidate == leastRelevantMultiple;
    }

    private boolean isNotMultipleOfAnyPreviousPrimeFactor(int candidate) {
        for (int n = 1; n < multiplesOfPrimeFactors.size(); n++) {
            if (isMultipleOfNthPrimeFactor(candidate, n)) {
                return false;
            }
        }
        return true;
    }

    private boolean isMultipleOfNthPrimeFactor(int candidate, int n) {
        return candidate == smallestOddNthMultipleNotLessThanCandidate(candidate, n);
    }

    private int smallestOddNthMultipleNotLessThanCandidate(int candidate, int n) {
        int multiple = multiplesOfPrimeFactors.get(n);
        while (multiple < candidate) {
            multiple += 2 * primes.get(n);
        }
        multiplesOfPrimeFactors.set(n, multiple);
        return multiple;
    }
}
