# Sieve of Eratosthenes

Based on *Listing 4-7, 4-8*

My attempt at writing the *Sieve of Eratosthenes* with the following improvements:
  1. Make the algorithm stateless
  2. Extract an interface for generating prime numbers in order to swap in different implementations
  3. Make use of higher-level Java APIs

Also, added the following enhancements:
  - Basic tests
  - Command line interface

## Attempts

- `SieveOfEratosthenes1` is my original attempt at writing the algorithm.
- `SieveOfEratosthenes2` is my attempt at refactoring the class based on the guidelines of *Clean Code, Chapters 1-4*.
- `SieveOfEratosthenes3` is my attempt at abstracting the way the data is represented from the algorithm.
  This resulted in extracting the number array and various methods to a new class called `NumberBuffer`.

**Note:**
The third refactoring makes use of `Stream.takeWhile()`, which was added in Java 9.

## Command Line (via Gradle wrapper)

    $ ./gradlew -q run --args=<maxValue>

## Unit Tests

    $ ./gradlew cleanTest test

The `cleanTest` task forces the tests to re-run, so you get test pass/fail information output.
Otherwise, check `build/report/tests/test/index.html` for previously run results.