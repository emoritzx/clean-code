package me.abiogenesis.cleancode.sieve;

import java.util.List;

public interface PrimeGenerator {
    List<Integer> generatePrimes(int maxValue);
}
