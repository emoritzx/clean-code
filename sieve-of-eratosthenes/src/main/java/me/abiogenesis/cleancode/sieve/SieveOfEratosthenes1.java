package me.abiogenesis.cleancode.sieve;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Implements the Sieve of Eratosthenes
 *
 * @see {@link https://en.wikipedia.org/wiki/Sieve_of_Eratosthenes}
 */
public class SieveOfEratosthenes1 implements PrimeGenerator {

    private static final int MIN_VALUE = 2;

    @Override
    public List<Integer> generatePrimes(int maxValue) {
        if (maxValue < MIN_VALUE) {
            return Collections.emptyList();
        }
        // detect composite numbers
        boolean[] compositeNumbers = new boolean[maxValue + 1];
        for (int base = MIN_VALUE; base <= maxValue; ++base) {
            for (int multipleOfBase = base * 2; multipleOfBase <= maxValue; multipleOfBase += base) {
                compositeNumbers[multipleOfBase] = true;
            }
        }
        // scan for prime numbers
        return IntStream.rangeClosed(MIN_VALUE, maxValue)
            .filter(index -> !compositeNumbers[index])
            .boxed()
            .collect(Collectors.toList());
    }
}