package me.abiogenesis.cleancode.sieve;

import java.util.List;
import java.util.function.IntPredicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class NumberBuffer {

    private static final int MIN_VALUE = 2;

    private final boolean[] markedNumberIndices;

    public NumberBuffer(int maxValue) {
        if (maxValue < MIN_VALUE) {
            throw new IllegalArgumentException(String.format("maxValue cannot be less than %s", MIN_VALUE));
        }
        this.markedNumberIndices = new boolean[maxValue + 1];
    }

    public void mark(int number) {
        markedNumberIndices[number] = true;
    }

    public int getMinValue() {
        return MIN_VALUE;
    }

    public int getMaxValue() {
        return markedNumberIndices.length - 1;
    }

    public List<Integer> collect(IntPredicate numberFilter) {
        return IntStream.rangeClosed(MIN_VALUE, getMaxValue())
            .filter(numberFilter)
            .boxed()
            .collect(Collectors.toList());
    }

    public IntPredicate marked() {
        return index -> markedNumberIndices[index];
    }

    public IntPredicate unmarked() {
        return marked().negate();
    }
}