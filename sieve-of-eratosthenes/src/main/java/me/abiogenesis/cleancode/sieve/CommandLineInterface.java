package me.abiogenesis.cleancode.sieve;

import java.util.List;

public class CommandLineInterface {

    public static void main(String[] args) {
        if (args.length != 1) {
            throw new IllegalArgumentException("Expected a single argument: maxValue");
        }
        int maxValue = Integer.parseInt(args[0]);
        System.out.printf("Generating primes less than %d%n", maxValue);
        PrimeGenerator generator = new SieveOfEratosthenes3();
        List<Integer> primes = generator.generatePrimes(maxValue);
        primes.forEach(System.out::println);
    }
}
