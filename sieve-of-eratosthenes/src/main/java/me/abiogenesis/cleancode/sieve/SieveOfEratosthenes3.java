package me.abiogenesis.cleancode.sieve;

import java.util.Collections;
import java.util.List;
import java.util.stream.IntStream;

/**
 * Applied additional refactorings of {@link SieveOfEratosthenes2} by extracting out the data representation (a number buffer) from the algorithm logic.
 */
public class SieveOfEratosthenes3 implements PrimeGenerator {

    @Override
    public List<Integer> generatePrimes(int maxValue) {
        try {
            NumberBuffer numbers = new NumberBuffer(maxValue);
            markCompositeNumbers(numbers);
            return numbers.collect(numbers.unmarked());
        } catch (IllegalArgumentException ex) {
            return Collections.emptyList();
        }
    }

    private void markCompositeNumbers(NumberBuffer numbers) {
        IntStream.rangeClosed(numbers.getMinValue(), numbers.getMaxValue())
            .forEach(factor -> markMultiplesOf(factor, numbers));
    }

    private void markMultiplesOf(int factor, NumberBuffer numbers) {
        IntStream.iterate(2, multiplier -> multiplier + 1)
            .map(multiplier -> factor * multiplier)
            .takeWhile(multiple -> multiple <= numbers.getMaxValue())
            .forEach(numbers::mark);
    }
}