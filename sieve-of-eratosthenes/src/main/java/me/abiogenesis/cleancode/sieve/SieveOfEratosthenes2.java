package me.abiogenesis.cleancode.sieve;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Applied refactorings of {@link SieveOfEratosthenes1} based on the recommendations of Clean Code, Chapters 1-4.
 */
public class SieveOfEratosthenes2 implements PrimeGenerator {

    private static final int MIN_VALUE = 2;

    @Override
    public List<Integer> generatePrimes(int maxValue) {
        if (isOutOfBounds(maxValue)) {
            return Collections.emptyList();
        }
        boolean[] compositeNumberIndices = generateCompositeNumberBuffer(maxValue);
        crossOutCompositeNumbers(compositeNumberIndices);
        return collectPrimeNumbers(compositeNumberIndices);
    }

    private boolean isOutOfBounds(int maxValue) {
        return maxValue < MIN_VALUE;
    }

    private boolean[] generateCompositeNumberBuffer(int maxValue) {
        return new boolean[maxValue + 1];
    }

    private void crossOutCompositeNumbers(boolean[] compositeNumberIndices) {
        int maxValue = compositeNumberIndices.length - 1;
        for (int factor = MIN_VALUE; factor <= maxValue; ++factor) {
            crossOutMultiplesOf(factor, compositeNumberIndices);
        }
    }

    private void crossOutMultiplesOf(int factor, boolean[] compositeNumberIndices) {
        int maxValue = compositeNumberIndices.length - 1;
        for (int multipleOfFactor = factor * 2; multipleOfFactor <= maxValue; multipleOfFactor += factor) {
            compositeNumberIndices[multipleOfFactor] = true;
        }
    }

    private List<Integer> collectPrimeNumbers(boolean[] compositeNumberIndices) {
        int maxValue = compositeNumberIndices.length - 1;
        return IntStream.rangeClosed(MIN_VALUE, maxValue)
            .filter(index -> !compositeNumberIndices[index])
            .boxed()
            .collect(Collectors.toList());
    }
}