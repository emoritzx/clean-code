package me.abiogenesis.cleancode.sieve;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

import static org.testng.Assert.assertEquals;

public class PrimeGeneratorTests {

    @DataProvider
    public static Object[][] primesData() {
        return new Object[][] {
            { -1, Collections.emptyList() },
            {  0, Collections.emptyList() },
            {  1, Collections.emptyList() },
            {  2, Arrays.asList(2) },
            {  3, Arrays.asList(2, 3) },
            { 10, Arrays.asList(2, 3, 5, 7) },
            { 30, Arrays.asList(2, 3, 5, 7, 11, 13, 17, 19, 23, 29) },
            { 31, Arrays.asList(2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31) }
        };
    }

    @DataProvider
    public static Object[][] generators() {
        return Stream.of(
                new SieveOfEratosthenes1(),
                new SieveOfEratosthenes2(),
                new SieveOfEratosthenes3())
            .flatMap(generator ->
                Arrays.stream(primesData())
                    .map(testCase -> new Object[] { generator, testCase[0], testCase[1]}))
            .toArray(Object[][]::new);
    }

    @Test(dataProvider = "generators")
    public void testGeneratePrimes(PrimeGenerator generator, int maxValue, List<Integer> expectedPrimes) {
        List<Integer> actualPrimes = generator.generatePrimes(maxValue);
        assertEquals(actualPrimes, expectedPrimes, "Prime numbers");
    }
}